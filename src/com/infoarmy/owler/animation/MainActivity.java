package com.infoarmy.owler.animation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.nineoldandroids.animation.Animator.AnimatorListener;

public class MainActivity extends Activity {

	private ListView listview;
	final String[] values = new String[] { "Android", "iPhone",
			"WindowsMobile", "Blackberry", "WebOS", "Ubuntu", "Windows7",
			"Max OS X", "Linux", "OS/2", "Ubuntu", "Windows7", "Max OS X",
			"Linux", "OS/2", "Ubuntu", "Windows7", "Max OS X", "Linux", "OS/2",
			"Android", "iPhone", "WindowsMobile" };
	private Intent i;
	private int height;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initalize();
		setUpListeners();

	}

	private void setUpListeners() {
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(final AdapterView<?> parent,
					final View view, int position, long id) {

				int currentapiVersion = android.os.Build.VERSION.SDK_INT;
				System.out.println("currentapiVersion" + currentapiVersion);
				if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
					for (int i = 0; i <= listview.getLastVisiblePosition(); i++) {
						aniamteTheListViewWithDefaultAPIs(i, position);
						System.out.println("Default");
					}
				} else {
					for (int i = 0; i <= listview.getLastVisiblePosition(); i++) {
						aniamteTheListViewWithNineOldAndroidAPIs(i, position);
						System.out.println("Default");
					}

				}

			}

		});
	}

	private void initalize() {
		Display d = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
				.getDefaultDisplay();

		height = d.getHeight() * 2;

		i = new Intent(MainActivity.this, NextActivity.class);

		listview = (ListView) findViewById(R.id.listview);

		final ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < values.length; ++i) {
			list.add(values[i]);
		}

		final StableArrayAdapter adapter = new StableArrayAdapter(this,
				android.R.layout.simple_list_item_1, list);
		listview.setAdapter(adapter);
	}

	@Override
	protected void onResume() {
		initalize();
		super.onResume();
	}

	private void aniamteTheListViewWithNineOldAndroidAPIs(int i, int position) {

		System.out.println("i" + i);
		System.out.println("clicked postion" + position);

		int visiblePosition = listview.getFirstVisiblePosition();

		final View view = listview.getChildAt(i - visiblePosition);

		if (view == null)
			return;
		if (i < position) {
			com.nineoldandroids.animation.ObjectAnimator anim = com.nineoldandroids.animation.ObjectAnimator
					.ofFloat(view, "y", view.getTop(), view.getTop() - height);
			anim.setDuration(1500);
			anim.start();
		} else {
			if (i == position) {
				com.nineoldandroids.animation.ObjectAnimator anim = com.nineoldandroids.animation.ObjectAnimator
						.ofFloat(view, "alpha", 0);
				anim.setDuration(700);

				anim.addListener(new AnimatorListener() {

					@Override
					public void onAnimationStart(
							com.nineoldandroids.animation.Animator arg0) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationRepeat(
							com.nineoldandroids.animation.Animator arg0) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(

					com.nineoldandroids.animation.Animator arg0) {
						moveToNewActivity();
						view.setVisibility(View.INVISIBLE);

					}

					@Override
					public void onAnimationCancel(
							com.nineoldandroids.animation.Animator arg0) {

					}
				});
				anim.start();
			} else {
				com.nineoldandroids.animation.ObjectAnimator anim = com.nineoldandroids.animation.ObjectAnimator
						.ofFloat(view, "y", view.getTop(), view.getTop()
								+ height);
				anim.setDuration(1500);
				anim.start();

			}
		}
	}

	private void aniamteTheListViewWithDefaultAPIs(int i, int postion) {

		System.out.println("i" + i);
		System.out.println("clicked postion" + postion);

		int visiblePosition = listview.getFirstVisiblePosition();

		final View view = listview.getChildAt(i - visiblePosition);

		if (view == null)
			return;
		if (i < postion) {
			ObjectAnimator anim = ObjectAnimator.ofFloat(view, "y",
					view.getY(), view.getY() - height);
			anim.setDuration(1500);

			view.setHasTransientState(true);
			anim.start();
		} else {
			if (i == postion) {
				ObjectAnimator anim = ObjectAnimator.ofFloat(view, View.ALPHA,
						0);
				anim.setDuration(400);
				view.setHasTransientState(true);
				anim.addListener(new AnimatorListenerAdapter() {

					@Override
					public void onAnimationEnd(Animator animation) {
						moveToNewActivity();
						view.setAlpha(1);
						view.setHasTransientState(true);
						view.setVisibility(View.INVISIBLE);
					}
				});
				anim.start();
			} else {
				ObjectAnimator anim = ObjectAnimator.ofFloat(view, "y",
						view.getY(), view.getY() + height);
				view.setHasTransientState(true);
				anim.setDuration(1500);
				anim.start();

			}

		}
	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}

	private void moveToNewActivity() {
		startActivity(i);
		overridePendingTransition(R.anim.fadein, R.anim.fadeout);

	}
}
